# Quick EKS or k8s cluster on AWS via eksctl

Here's what I did:
I used our https://gitlabsandbox.cloud/cloud to open my AWS console that was previously provisioned.
In the AWS Console, I opened a AWS CloudShell
In the AWS CloudShell, I typed:
```shell
aws sts get-caller-identity
# install eksctl:
ARCH=amd64
PLATFORM=$(uname -s)_$ARCH
curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_$PLATFORM.tar.gz"
# (Optional) Verify checksum
curl -sL "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_checksums.txt" | grep $PLATFORM | sha256sum --check
tar -xzf eksctl_$PLATFORM.tar.gz -C /tmp && rm eksctl_$PLATFORM.tar.gz
sudo mv /tmp/eksctl /usr/local/bin
eksctl create cluster --name my-cluster --region us-east-2 # Note I am not using Fargate. Fargate fails.
kubectl get nodes -A
kubectl get pods -A -o wide
# Install helm:
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
helm
# Now use the GitLab Web UI to Retrieve the directions to connect to our repo/cluster agent thing:
# Project > Operate > Kubernetes Clusters. Click [Connect a Cluster].
# Pull-down 'simplenotes' and click [Create]
# Copy and pasta the second link with the helm command into your AWS CloudShell.
# For example, my command was:
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install simplenotes gitlab/gitlab-agent     --namespace gitlab-agent-simplenotes     --create-namespace     --set image.tag=v16.8.0     --set config.token=glagent-vB-ZysADmpqBTwcWBZ7mjtQYeazxDwszDpJUbPFjTF7SmnCsxA     --set config.kasAddress=wss://kas.gitlab.com

kubectl get pods -A
```
# Done!

Now we can deploy the simplenotes application per the directions, here:
https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnera[…]es/getting_started/lesson_3_deploying_the_demo_application/
AFter deployment, you can then do the demo, here:
https://gitlab-de.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnera[…]ting_up_and_configuring_the_security_scanners_and_policies/